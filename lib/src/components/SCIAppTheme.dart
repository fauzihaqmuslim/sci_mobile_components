import 'package:flutter/material.dart';

class SCIThemeModel {
  // Primary color used by the component
  final Color primaryColor;

  // Secondary or accent color
  final Color secondaryColor;

  // Light color mainly used for background
  final Color lightColor;

  // Dark color mainly used for text
  final Color darkColor;

  SCIThemeModel({
    this.primaryColor,
    this.secondaryColor,
    this.lightColor,
    this.darkColor,
  });
}

class SCIAppTheme extends InheritedWidget {
  // The for all the component
  final SCIThemeModel theme;

  // Child widget that use this theme
  final Widget child;

  SCIAppTheme({
    @required this.theme,
    this.child,
  });

  // Rebuild the dependant widget if theme change
  @override
  bool updateShouldNotify(SCIAppTheme oldWidget) => oldWidget.theme != theme;

  static SCIAppTheme of(BuildContext context) =>
      context.dependOnInheritedWidgetOfExactType<SCIAppTheme>();
}
