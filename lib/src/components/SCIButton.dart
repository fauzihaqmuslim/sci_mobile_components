import 'package:flutter/material.dart';
import 'package:sci_mobile_components/src/components/SCIAppTheme.dart';

enum SCIButtonType { filled, outline }

class SCIButton extends StatelessWidget {
  final VoidCallback onPressed;
  final SCIButtonType type;
  final String text;
  final bool isLoading;
  final Color color;

  SCIButton({
    @required this.onPressed,
    this.type = SCIButtonType.filled,
    this.text,
    this.isLoading = false,
    this.color,
  });

  @override
  Widget build(BuildContext context) {
    switch (type) {
      case SCIButtonType.filled:
        return _FilledButton(
          onPressed: onPressed,
          text: text,
          isLoading: isLoading,
          color: color,
        );
        break;
      case SCIButtonType.outline:
        return _OutlineButton(
          onPressed: onPressed,
          text: text,
          isLoading: isLoading,
          color: color,
        );
        break;
      default:
        return _FilledButton(
          onPressed: onPressed,
          text: text,
          isLoading: isLoading,
          color: color,
        );
        break;
    }
  }
}

class _OutlineButton extends StatelessWidget {
  final VoidCallback onPressed;
  final String text;
  final bool isLoading;
  final Color color;

  const _OutlineButton({
    @required this.onPressed,
    this.text,
    this.isLoading,
    this.color,
  });

  @override
  Widget build(BuildContext context) {
    // Extract theme properties from top level SCIAppTheme
    final SCIThemeModel theme = SCIAppTheme.of(context).theme;

    return SizedBox(
      height: 50.0,
      width: double.infinity,
      child: OutlineButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        borderSide: BorderSide(
          color: onPressed == null ? Colors.grey : color ?? theme.primaryColor,
          width: 2.0,
        ),
        padding: EdgeInsets.all(10.0),
        child: isLoading
            ? CircularProgressIndicator()
            : Text(
                text ?? '',
                style: TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.w600,
                  fontFamily: 'Overpass',
                  color: onPressed != null
                      ? color ?? theme.primaryColor
                      : Colors.grey,
                ),
              ),
        onPressed: onPressed,
        highlightedBorderColor: color ?? theme.primaryColor,
        highlightColor: Colors.grey[300],
      ),
    );
  }
}

class _FilledButton extends StatelessWidget {
  final VoidCallback onPressed;
  final String text;
  final bool isLoading;
  final Color color;

  const _FilledButton({
    @required this.onPressed,
    this.text,
    this.isLoading,
    this.color,
  });

  @override
  Widget build(BuildContext context) {
    // Extract theme properties from top level SCIAppTheme
    final SCIThemeModel theme = SCIAppTheme.of(context).theme;

    return SizedBox(
      width: double.infinity,
      height: 50.0,
      child: FlatButton(
        color: color ?? theme.primaryColor,
        disabledColor: Colors.grey[400],
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        padding: EdgeInsets.all(10.0),
        child: isLoading
            ? CircularProgressIndicator()
            : Text(
                text ?? '',
                style: TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.w600,
                  fontFamily: 'Overpass',
                  color: onPressed != null ? theme.lightColor : Colors.grey,
                ),
              ),
        onPressed: onPressed,
      ),
    );
  }
}
