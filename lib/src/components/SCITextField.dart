import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:sci_mobile_components/src/components/SCIAppTheme.dart';

enum SCITextFieldType { normal, phone, multiline }

class SCITextField extends StatelessWidget {
  final TextEditingController controller;
  final SCITextFieldType type;
  final String placeholder;
  final int maxLines;
  final int maxLength;
  final TextInputType inputType;
  final bool obscureText;

  SCITextField({
    @required this.controller,
    this.type = SCITextFieldType.normal,
    this.placeholder,
    this.maxLines,
    this.maxLength = 255,
    this.inputType,
    this.obscureText = false,
  });
  @override
  Widget build(BuildContext context) {
    switch (type) {
      case SCITextFieldType.multiline:
        return _MultilineTextField(
          controller: controller,
          maxLines: maxLines,
          maxLength: maxLength,
          placeholder: placeholder,
        );
        break;
      case SCITextFieldType.phone:
        return _PhoneTextField(
          controller: controller,
          placeholder: placeholder,
        );
        break;
      case SCITextFieldType.normal:
        return _NormalTextField(
          controller: controller,
          placeholder: placeholder,
          inputType: inputType,
          obscureText: obscureText,
        );
        break;
      default:
        return _NormalTextField(
          controller: controller,
          placeholder: placeholder,
          inputType: inputType,
          obscureText: obscureText,
        );
        break;
    }
  }
}

class _NormalTextField extends StatelessWidget {
  final TextEditingController controller;
  final String placeholder;
  final TextInputType inputType;
  final bool obscureText;

  const _NormalTextField({
    @required this.controller,
    @required this.placeholder,
    this.inputType = TextInputType.text,
    this.obscureText,
  });

  @override
  Widget build(BuildContext context) {
    // Extract theme properties from top level SCIAppTheme
    final SCIThemeModel theme = SCIAppTheme.of(context).theme;

    return TextField(
      obscureText: obscureText,
      keyboardType: inputType,
      controller: controller,
      style:
          Theme.of(context).textTheme.title.copyWith(color: theme.primaryColor),
      decoration: InputDecoration(
        filled: true,
        fillColor: Colors.grey[200],
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: Colors.grey,
            width: 2.0,
          ),
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: theme.primaryColor,
            width: 2.0,
          ),
        ),
        hintText: placeholder,
      ),
    );
  }
}

class _PhoneTextField extends StatelessWidget {
  final TextEditingController controller;
  final String placeholder;

  const _PhoneTextField({
    Key key,
    @required this.controller,
    @required this.placeholder,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // Extract theme properties from top level SCIAppTheme
    final SCIThemeModel theme = SCIAppTheme.of(context).theme;

    return TextField(
      maxLength: 11,
      controller: controller,
      keyboardType: TextInputType.phone,
      style:
          Theme.of(context).textTheme.title.copyWith(color: theme.primaryColor),
      decoration: InputDecoration(
        filled: true,
        fillColor: Colors.grey[200],
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: Colors.grey,
            width: 2.0,
          ),
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: theme.primaryColor,
            width: 2.0,
          ),
        ),
        hintText: placeholder,
        prefixIcon: _PhoneNumPrefix(),
      ),
    );
  }
}

class _MultilineTextField extends StatelessWidget {
  final TextEditingController controller;
  final int maxLines;
  final int maxLength;
  final String placeholder;

  const _MultilineTextField({
    Key key,
    @required this.controller,
    @required this.maxLines,
    @required this.maxLength,
    @required this.placeholder,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // Extract theme properties from top level SCIAppTheme
    final SCIThemeModel theme = SCIAppTheme.of(context).theme;

    return TextField(
      keyboardType: TextInputType.multiline,
      controller: controller,
      maxLines: maxLines,
      maxLength: maxLength,
      style:
          Theme.of(context).textTheme.title.copyWith(color: theme.primaryColor),
      decoration: InputDecoration(
        filled: true,
        fillColor: Colors.grey[200],
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: Colors.grey,
            width: 2.0,
          ),
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: theme.primaryColor,
            width: 2.0,
          ),
        ),
        hintText: placeholder,
        counterStyle: TextStyle(
          color: theme.primaryColor,
        ),
      ),
    );
  }
}

class _PhoneNumPrefix extends StatelessWidget {
  const _PhoneNumPrefix({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // Extract theme properties from top level SCIAppTheme
    final SCIThemeModel theme = SCIAppTheme.of(context).theme;

    return SizedBox(
      height: 40.0,
      width: 60.0,
      child: Center(
        child: Text(
          '+62',
          style: Theme.of(context)
              .textTheme
              .title
              .copyWith(color: theme.primaryColor),
        ),
      ),
    );
  }
}
