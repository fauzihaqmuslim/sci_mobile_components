import 'package:flutter/material.dart';
import 'package:sci_mobile_components/sci_mobile_components.dart';

class SCIMenuItem extends StatelessWidget {
  final IconData icon;
  final String label;
  final VoidCallback onPressed;
  final bool withBorder;

  SCIMenuItem({
    this.icon,
    this.label,
    @required this.onPressed,
    this.withBorder,
  });

  @override
  Widget build(BuildContext context) {
    SCIThemeModel theme = SCIAppTheme.of(context).theme;

    return GestureDetector(
      onTap: onPressed,
      child: FractionallySizedBox(
        widthFactor: 0.3,
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 2.0, vertical: 8.0),
          decoration: withBorder == true
              ? BoxDecoration(
                  border: Border.all(color: Colors.white, width: 2.0),
                  borderRadius: BorderRadius.circular(6.0),
                )
              : null,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(
                icon,
                size: 42.0,
                color: Color.fromRGBO(255, 255, 255, 1),
              ),
              SizedBox(width: 10.0),
              Text(
                label ?? '',
                style: Theme.of(context).textTheme.subhead.copyWith(
                      color: theme.lightColor,
                      fontWeight: FontWeight.w600,
                    ),
                textAlign: TextAlign.center,
              )
            ],
          ),
        ),
      ),
    );
  }
}
