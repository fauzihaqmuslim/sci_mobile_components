import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';

class BloodPressure {
  final int time;
  final int systolic;
  final int diastolic;

  BloodPressure(this.time, this.systolic, this.diastolic);
}

class SCIVitalSign extends StatelessWidget {
  final List<BloodPressure> data = [
    BloodPressure(0, 130, 88),
    BloodPressure(1, 117, 79),
    BloodPressure(2, 122, 84),
    BloodPressure(3, 133, 79),
    BloodPressure(4, 121, 76),
    BloodPressure(5, 123, 78),
    BloodPressure(6, 138, 90),
  ];

  @override
  Widget build(BuildContext context) {
    return FractionallySizedBox(
      heightFactor: 0.6,
      child: Container(
        padding: EdgeInsets.all(20.0),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(18.0),
          color: Colors.white,
        ),
        child: LineChart(
          LineChartData(
            clipToBorder: false,
            gridData: FlGridData(horizontalInterval: 10.0),
            titlesData: FlTitlesData(
              show: false,
            ),
            borderData: FlBorderData(
              show: true,
              border: Border.all(
                color: Colors.blue,
                width: 3.0,
              ),
            ),
            minY: 40,
            maxY: 160,
            lineBarsData: [
              // Batas atas
              LineChartBarData(
                colors: [Colors.yellow.withOpacity(0.3)],
                show: true,
                spots: [
                  for (double i = 0; i < data.length; i++) FlSpot(i, 120),
                ],
                aboveBarData: BarAreaData(
                  show: true,
                  colors: [Colors.yellow.withOpacity(0.3)],
                ),
                dotData: FlDotData(
                  show: false,
                ),
              ),
              // Batas bawah
              LineChartBarData(
                colors: [Colors.yellow.withOpacity(0.3)],
                show: true,
                spots: [
                  for (double i = 0; i < data.length; i++) FlSpot(i, 80),
                ],
                belowBarData: BarAreaData(
                  show: true,
                  colors: [Colors.yellow.withOpacity(0.3)],
                ),
                dotData: FlDotData(
                  show: false,
                ),
              ),
              // Systolic
              LineChartBarData(
                dotData: FlDotData(dotColor: Colors.redAccent),
                show: true,
                barWidth: 3.0,
                spots: data
                    .map(
                      (bp) => FlSpot(
                        bp.time.toDouble(),
                        bp.systolic.toDouble(),
                      ),
                    )
                    .toList(),
              ),
              // Diastolic
              LineChartBarData(
                colors: [Colors.blue],
                show: true,
                barWidth: 3.0,
                spots: data
                    .map(
                      (bp) => FlSpot(
                        bp.time.toDouble(),
                        bp.diastolic.toDouble(),
                      ),
                    )
                    .toList(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
