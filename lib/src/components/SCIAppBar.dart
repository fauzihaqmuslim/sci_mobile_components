import 'package:flutter/material.dart';
import 'package:sci_mobile_components/src/components/SCIAppTheme.dart';
import 'package:sci_mobile_components/src/components/SCIBody.dart';

class SCIAppBar extends StatelessWidget implements PreferredSizeWidget {
  final double height;
  final Image logo;
  final VoidCallback onSettingPressed;
  final VoidCallback onNotificationPressed;

  SCIAppBar({
    this.height = 60.0,
    this.logo,
    this.onSettingPressed,
    this.onNotificationPressed,
  });

  @override
  Widget build(BuildContext context) {
    // Extract theme properties from top level SCIAppTheme
    final SCIThemeModel theme = SCIAppTheme.of(context).theme;

    return Container(
      color: theme.lightColor,
      child: SafeArea(
        child: SCIBody(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              logo,
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  IconButton(
                    icon: Icon(Icons.notifications),
                    onPressed: onNotificationPressed,
                    iconSize: 24.0,
                    color: theme.darkColor,
                  ),
                  IconButton(
                    icon: Icon(Icons.settings),
                    onPressed: onSettingPressed,
                    iconSize: 24.0,
                    color: theme.darkColor,
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(height);
}
