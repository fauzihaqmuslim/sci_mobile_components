import 'package:flutter/material.dart';
import 'package:sci_mobile_components/src/components/SCIAppTheme.dart';

class SCIOption extends StatelessWidget {
  final String label;
  final String value;
  final String fallbackValue;
  final VoidCallback onPressed;

  SCIOption({
    @required this.label,
    @required this.value,
    @required this.fallbackValue,
    @required this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    // Extract theme properties from top level SCIAppTheme
    final SCIThemeModel theme = SCIAppTheme.of(context).theme;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Text(
          label,
          style: Theme.of(context).textTheme.subhead.copyWith(
                fontFamily: 'Overpass',
                color: theme.darkColor,
              ),
        ),
        SizedBox(
          height: 4.0,
        ),
        GestureDetector(
          onTap: () => onPressed(),
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 10.0),
            height: 45.0,
            decoration: BoxDecoration(
              color: Colors.grey[200],
              border: Border.all(
                color: value != null ? theme.primaryColor : Colors.grey,
                width: 2.0,
              ),
              borderRadius: BorderRadius.circular(4.0),
            ),
            alignment: Alignment.centerLeft,
            child: Text(
              value ?? fallbackValue,
              style: Theme.of(context).textTheme.title.copyWith(
                    fontFamily: 'Overpass',
                    color: value != null ? theme.primaryColor : Colors.grey,
                  ),
            ),
          ),
        ),
      ],
    );
  }
}

class SCIOptionDialog extends StatelessWidget {
  final Widget child;

  const SCIOptionDialog({
    @required this.child,
  });

  @override
  Widget build(BuildContext context) {
    SCIThemeModel theme = SCIAppTheme.of(context).theme;
    double dHeight = MediaQuery.of(context).size.height;
    double dWidth = MediaQuery.of(context).size.width;

    return Dialog(
      backgroundColor: theme.lightColor,
      elevation: 2.0,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
      child: ConstrainedBox(
        constraints: BoxConstraints(
          minHeight: dHeight * 0.5,
          maxHeight: dHeight * 0.7,
          minWidth: dWidth * 0.9,
          maxWidth: dWidth * 0.9,
        ),
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Container(
            child: child,
          ),
        ),
      ),
    );
  }
}
