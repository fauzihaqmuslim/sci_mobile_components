import 'package:flutter/material.dart';
import 'package:sci_mobile_components/src/components/SCIAppTheme.dart';

class SCIBody extends StatelessWidget {
  final Widget child;

  SCIBody({
    this.child,
  });

  @override
  Widget build(BuildContext context) {
    // Extract theme properties from top level SCIAppTheme
    final SCIThemeModel theme = SCIAppTheme.of(context).theme;

    return Container(
      color: theme.lightColor,
      alignment: Alignment.center,
      child: FractionallySizedBox(
        heightFactor: 0.9,
        widthFactor: 0.85,
        child: child,
      ),
    );
  }
}
