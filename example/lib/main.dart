import 'package:flutter/material.dart';
import 'package:sci_mobile_components/sci_mobile_components.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) {
    return SCIAppTheme(
      theme: SCIThemeModel(
        primaryColor: Color.fromRGBO(57, 151, 203, 1),
        secondaryColor: Color.fromRGBO(229, 144, 66, 1),
        lightColor: Color.fromRGBO(255, 255, 255, 1),
        darkColor: Color.fromRGBO(64, 64, 64, 1),
      ),
      child: MaterialApp(
        home: Scaffold(
          appBar: SCIAppBar(
            logo: Image.network('https://via.placeholder.com/140x40'),
            onSettingPressed: () {},
            onNotificationPressed: () {},
          ),
          body: SCITextField(
            controller: TextEditingController(),
            type: SCITextFieldType.phone,
          ),
        ),
      ),
    );
  }
}

class Test extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          padding: EdgeInsets.all(20.0),
          width: double.infinity,
          color: Colors.blue,
          child: Wrap(
            alignment: WrapAlignment.spaceBetween,
            runSpacing: 20.0,
            children: <Widget>[
              SCIMenuItem(
                icon: Icons.train,
                label: 'testing saja inisih',
                onPressed: () {},
              ),
              SCIMenuItem(
                icon: Icons.train,
                label: 'Tanda Vital',
                onPressed: () {},
              ),
              SCIMenuItem(
                icon: Icons.train,
                label: 'testing saja',
                onPressed: () {},
              ),
              SCIMenuItem(
                icon: Icons.train,
                label: 'testing saja',
                onPressed: () {},
              ),
              SCIMenuItem(
                icon: Icons.train,
                label: 'testing saja',
                onPressed: () {},
              ),
              SCIMenuItem(
                icon: Icons.train,
                label: 'testing saja',
                onPressed: () {},
              ),
              SCIMenuItem(
                icon: Icons.train,
                label: 'testing saja',
                onPressed: () {},
              ),
            ],
          ),
        )
      ],
    );
  }
}
